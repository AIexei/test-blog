from django.contrib.auth import get_user_model
from rest_framework import serializers

from blog.apps.authentication.models import Profile


class RegistrationSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, trim_whitespace=False)

    class Meta:
        model = get_user_model()
        fields = ('username', 'password', 'email')
        extra_kwargs = {'email': {'required': True}}

    def create(self, validated_data):
        """Method for creating user."""
        user_instance = get_user_model().objects.create_user(
            validated_data['username'],
            email=validated_data['email'],
            password=validated_data['password'],
        )

        return user_instance


class UserSerializer(serializers.ModelSerializer):
    """Serializer for User model."""

    class Meta:
        model = get_user_model()
        fields = ('id', 'username', 'email')


class ProfileSerializer(serializers.ModelSerializer):
    """Serializer for Profile models."""

    user = UserSerializer(read_only=True)
    posts_count = serializers.IntegerField(read_only=True)
    followers_count = serializers.IntegerField(read_only=True)
    followings_count = serializers.IntegerField(read_only=True)

    class Meta:
        model = Profile
        fields = (
            'id',
            'name',
            'user',
            'followers_count',
            'followings_count',
            'posts_count',
        )
