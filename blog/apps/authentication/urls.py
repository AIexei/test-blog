from django.urls import path, include
from rest_framework import routers
from rest_framework.authtoken import views as rest_views

from blog.apps.authentication.views import RegistrationView, ProfilesViewset


app_name = 'Authentication'

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'profiles', ProfilesViewset)

urlpatterns = [
    path('', include(router.urls)),
    path('login', rest_views.obtain_auth_token, name='login'),
    path('register', RegistrationView.as_view(), name='register'),
]
