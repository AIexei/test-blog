from django.apps import AppConfig


class AuthenticationConfig(AppConfig):
    name = 'blog.apps.authentication'

    def ready(self):
        from blog.apps.authentication import signals  # noqa: F401
