from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase


class AuthenticationTests(APITestCase):
    """Testing authentication."""

    def setUp(self):
        self.register_url = reverse('authentication:register')
        self.login_url = reverse('authentication:login')

        self.username = 'test'
        self.password = 'password'
        self.user = get_user_model().objects.create_user(self.username, password=self.password)

    def test_registration_validation(self):
        invalid_data = {'username': 'a', 'email': 'a'}
        response = self.client.post(self.register_url, invalid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_registration(self):
        valid_data = {'username': 'a', 'password': 'password', 'email': 'a@gmail.com'}
        previous_users_count = get_user_model().objects.count()

        response = self.client.post(self.register_url, valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.status_code)
        self.assertEqual(get_user_model().objects.count(), previous_users_count + 1, response.data)

    def test_login(self):
        response = self.client.post(self.login_url, {'username': 'x', 'password': 'x'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.status_code)

        valid_credentials = {'username': self.username, 'password': self.password}
        response = self.client.post(self.login_url, valid_credentials)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.status_code)
