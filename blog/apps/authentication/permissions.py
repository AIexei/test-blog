from rest_framework import permissions


class ProfilePermissions(permissions.IsAuthenticated):
    """Permissions for profile."""

    def has_object_permission(self, request, view, profile_obj):
        """Only read for everybody and all for owner."""
        return request.method in permissions.SAFE_METHODS or request.user == profile_obj.user
