from django.db.models import Count
from rest_framework import generics, permissions, response, status, viewsets, mixins, filters
from rest_framework.decorators import action
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import ValidationError

from blog.apps.authentication.models import Profile
from blog.apps.authentication.permissions import ProfilePermissions
from blog.apps.authentication.serializers import RegistrationSerializer, ProfileSerializer


class RegistrationView(generics.GenericAPIView):
    """View for processing registration."""

    permission_classes = (permissions.AllowAny,)
    serializer_class = RegistrationSerializer

    def post(self, request, *args, **kwargs):
        """Post method handler."""
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        token, _ = Token.objects.get_or_create(user=user)
        serialized_data = serializer.data
        serialized_data['token'] = token.key
        return response.Response(serialized_data, status=status.HTTP_201_CREATED)


class ProfilesViewset(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.DestroyModelMixin,
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet,
):
    """Viewset for profiles."""

    permission_classes = (ProfilePermissions,)
    serializer_class = ProfileSerializer
    queryset = Profile.objects.select_related('user').annotate(posts_count=Count('posts'))
    lookup_url_kwarg = 'username'
    lookup_field = 'user__username'
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('posts_count',)

    custom_actions = ['follow', 'unfollow']

    def get_permissions(self):
        """Is authenticated for custom actions."""
        if self.action in self.custom_actions:
            return [permissions.IsAuthenticated()]

        return super().get_permissions()

    def get_serializer(self, *args, **kwargs):
        """No serializer for custom actions."""
        if self.action in self.custom_actions:
            return None

        return super().get_serializer(*args, **kwargs)

    def get_object(self):
        """Can fetch yourself for custom actions."""
        if self.action in self.custom_actions:
            if self.request.user.username == self.kwargs[self.lookup_url_kwarg]:
                raise ValidationError('You cant follow/unfollow yourself')

        return super().get_object()

    @action(detail=True, methods=['post'])
    def follow(self, request, *args, **kwargs):
        """Follow user."""
        profile = self.get_object()
        current_profile = Profile.objects.get(user=request.user)
        current_profile.follow(profile)
        return response.Response({'status': 'OK'}, status=status.HTTP_200_OK)

    @action(detail=True, methods=['post'])
    def unfollow(self, request, *args, **kwargs):
        """Unfollow user."""
        profile = self.get_object()
        current_profile = Profile.objects.get(user=request.user)
        current_profile.unfollow(profile)
        return response.Response({'status': 'OK'}, status=status.HTTP_200_OK)
