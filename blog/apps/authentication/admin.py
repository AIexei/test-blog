from django.contrib import admin

from blog.apps.authentication.models import Profile


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    """Admin for Profile model."""

    list_display = ('username', 'name', 'followers_count', 'followings_count')
