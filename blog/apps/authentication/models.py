from django.db import models
from django.contrib.auth import get_user_model


class Profile(models.Model):  # noqa: Z214
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    name = models.CharField(max_length=250, blank=True)

    followings = models.ManyToManyField(
        'self',
        related_name='followers',
        symmetrical=False,
    )

    read_posts = models.ManyToManyField(
        'posts.Post',
        related_name='read_by',
    )

    @property
    def username(self):
        """Provides user's username."""
        return self.user.username

    @property
    def followers_count(self):
        """Amount of profile's followers."""
        return self.followers.count()

    @property
    def followings_count(self):
        """Amount followings of current profile."""
        return self.followings.count()

    def follow(self, profile):
        """Method for following another profile."""
        if not self.pk == profile.pk:
            self.followings.add(profile)

    def unfollow(self, profile):
        """Method for unfollowing another profile."""
        if not self.pk == profile.pk:
            self.followings.remove(profile)

    def read(self, post):
        """Method for read post."""
        if not self.pk == post.owner.pk:
            self.read_posts.add(post)

    def is_read_post(self, post):
        """Checks if post is read by current profile."""
        return self.read_posts.filter(post=post).exists() or self.pk == post.owner.pk  # noqa: Z221

    def __str__(self):
        return self.username
