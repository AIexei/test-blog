from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django_filters import rest_framework as filters

from blog.apps.authentication.models import Profile
from blog.apps.posts.models import Post


EMPTY_VALUES = (None, '', [], (), {})


class PostsFilter(filters.FilterSet):
    """Filterset for posts."""

    username = filters.CharFilter(method='filter_by_username')

    class Meta:  # noqa: D106
        model = Post
        fields = ()

    def filter_queryset(self, queryset):
        """Filter out empty usernames."""
        # handling username empty value here cause of filter_by_username wont be called when value is empty
        username_value = self.form.cleaned_data['username']
        if username_value in EMPTY_VALUES:
            queryset = queryset.filter(owner__user=self.request.user)

        return super().filter_queryset(queryset)

    def filter_by_username(self, queryset, name, username):
        """Correct filtering by username."""
        user = get_object_or_404(User, username=username)
        return queryset.filter(owner__user=user)


class FeedFilter(filters.FilterSet):
    """Filterset for feed."""

    unread_only = filters.BooleanFilter(method='filter_read_posts')

    class Meta:  # noqa: D106
        model = Post
        fields = ()

    def filter_read_posts(self, queryset, name, unread_only):
        """Filtering only unread posts."""
        if unread_only:
            read_posts_ids = Profile.objects.get(user=self.request.user).read_posts.values_list('id', flat=True)
            return queryset.exclude(id__in=read_posts_ids)

        return queryset
