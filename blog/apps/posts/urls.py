from django.urls import path, include
from rest_framework import routers

from blog.apps.posts.views import PostsViewset, FeedView

app_name = 'Posts'

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'posts', PostsViewset)

urlpatterns = [
    path('', include(router.urls)),
    path('feed', FeedView.as_view(), name='feed'),
]
