# Generated by Django 2.1.7 on 2019-03-15 09:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='subscription',
            name='follower',
        ),
        migrations.RemoveField(
            model_name='subscription',
            name='following',
        ),
        migrations.RemoveField(
            model_name='post',
            name='read_by',
        ),
        migrations.DeleteModel(
            name='Subscription',
        ),
    ]
