from django.apps import AppConfig


class PostsConfig(AppConfig):
    name = 'blog.apps.posts'
