from django.contrib import admin

from blog.apps.posts.models import Post


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    pass
