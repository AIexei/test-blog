from rest_framework import permissions


class PostsPermission(permissions.IsAuthenticated):
    """Permissions for posts."""

    def has_object_permission(self, request, view, post_obj):
        """Only safe for everybody and all for owner."""
        if request.method in permissions.SAFE_METHODS:
            return True

        return post_obj.owner.user == request.user
