from django.db import models


class Post(models.Model):
    owner = models.ForeignKey('authentication.Profile', related_name='posts', on_delete=models.CASCADE, db_index=True)
    title = models.CharField(max_length=100, null=False, blank=False)
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True, db_index=True)

    def is_read_by(self, profile):
        """Checks if post read by user."""
        return self.read_by.filter(profile=profile).exists()

    def __str__(self):
        return '{username} -- {time}'.format(username=self.owner.username, time=self.created_at)
