from rest_framework import viewsets, status, response, generics, permissions
from rest_framework.decorators import action

from blog.apps.authentication.models import Profile
from blog.apps.posts.models import Post
from blog.apps.posts.permissions import PostsPermission
from blog.apps.posts.serializers import PostSerializer
from blog.apps.posts.filters import PostsFilter, FeedFilter


class PostsViewset(viewsets.ModelViewSet):
    """Posts viewset."""

    serializer_class = PostSerializer
    permission_classes = (PostsPermission,)
    queryset = Post.objects.select_related('owner', 'owner__user').order_by('-created_at')
    filter_class = PostsFilter

    custom_actions = ['read']

    def get_permissions(self):
        """Is Authenticated for custom action."""
        if self.action in self.custom_actions:
            return [permissions.IsAuthenticated()]

        return super().get_permissions()

    def get_serializer(self, *args, **kwargs):
        """No serializer for custom action."""
        if self.action in self.custom_actions:
            return None

        return super().get_serializer(*args, **kwargs)

    def filter_queryset(self, queryset):
        """No filtering for custom action."""
        if self.action in self.custom_actions:
            return queryset

        return super().filter_queryset(queryset)

    @action(detail=True, methods=['post'])
    def read(self, request, *args, **kwargs):
        """Marks post as read."""
        post = self.get_object()
        current_profile = Profile.objects.get(user=request.user)
        current_profile.read_posts.add(post)
        return response.Response({'status': 'OK'}, status=status.HTTP_200_OK)


class FeedView(generics.ListAPIView):
    """Feed view."""

    serializer_class = PostSerializer
    permission_classes = (PostsPermission,)
    filter_class = FeedFilter

    def get_queryset(self):
        """Queryset based on followed profiles."""
        followed_profiles = Profile.objects.get(user=self.request.user).followings.all()

        queryset = Post.objects.select_related(
            'owner', 'owner__user',
        ).filter(
            owner__in=followed_profiles,
        ).order_by(
            '-created_at',
        )

        return queryset
