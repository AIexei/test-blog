from rest_framework import serializers

from blog.apps.authentication.serializers import ProfileSerializer
from blog.apps.authentication.models import Profile
from blog.apps.posts.models import Post


class PostSerializer(serializers.ModelSerializer):
    owner = ProfileSerializer(read_only=True)

    class Meta:
        model = Post
        fields = (
            'id',
            'owner',
            'title',
            'text',
            'created_at',
        )

    def create(self, validated_data):
        """Overriding create for passing profile."""
        profile = Profile.objects.get(user=self.context['request'].user)
        new_post = Post.objects.create(
            owner=profile,
            **validated_data,
        )

        return new_post
